@extends('base')
@section('content')

<div class="container">
    <h2>FOLID AC</h2>
    <form id="form" method="POST" action="registro " autocomplete="off">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <fieldset class=" scheduler-border">
            <legend class="scheduler-border">Registro de Asociados Activos</legend>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-stripped table-hover table-responsive">
                        <thead>
                        <th>#</th>
                        <th>Folio</th>
                        <th>Nombre</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        </thead>
                        @php( $i = 1 )
                        @foreach(ac\Asociado::All() as $c)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$c->folio}}</td>
                                <td>{{$c->nombre}}</td>
                                <td>{{$c->apellido_pat}}</td>
                                <td>{{$c->apellido_mat}}</td>

                            </tr>
                            @php( $i++)
                        @endforeach

                    </table>
                </div>
            </div>
        </fieldset>
    </form>
</div>
@endsection
