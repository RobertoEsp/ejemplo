<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsociadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asociados', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('folio_id')->unsigned();
            $table->string('folio')->unique();
            $table->string('nombre',100);
            $table->string('apellido_pat',100)->nullable();
            $table->string('apellido_mat',100)->nullable();
            $table->string('correo')->unique();
            $table->string('calle',100)->nullable();
            $table->string('colonia',100)->nullable();
            $table->string('municipio',100)->nullable();
            $table->string('estado',100)->nullable();
            $table->timestamps();
            $table->softDeletes();

            //$table->foreign('folio_id')->references('id')->on('folios');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asociados');
    }
}
