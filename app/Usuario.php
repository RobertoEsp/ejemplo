<?php

namespace ac;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';
    protected $primaryKey = 'id_usuario';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id_usuario',
        'usuario',
        'correo',
        'id_asociado',
        'id_cat_usuario',
    ];

    public function asociado(){
        return $this->hasOne('App\Asociado','id_asociado','id_asociado');
    }
    public function cat_usuario(){
        return $this->hasOne('App\Cat_usuarios','id_cat_usuario','id_cat_usuario');
    }
//    public function cat_usuario(){
//        return $this->hasMany('App\Cat_usuarios','id_cat_usuario','id_cat_usuario');
//    }
//    return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
}
