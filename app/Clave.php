<?php

namespace ac;

use Illuminate\Database\Eloquent\Model;

class Clave extends Model
{
    protected $table = 'claves';
    protected $primaryKey = 'id_clave';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id_clave',
        'clave',
        'fecha_ini',
        'fecha_fin',
        'id_usuario',
    ];

    public function cat_usuario(){
        return $this->hasOne('App\Cat_usuarios','id_cat_usuario','id_cat_usuario');
    }

}
