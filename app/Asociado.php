<?php

namespace ac;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asociado extends Model
{
    use SoftDeletes;

    protected $table = 'asociados';
    protected $primaryKey = 'id_asociado';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id_asociado',
        'id_folio',
        'nombre',
        'apellido_pat',
        'apellido_mat',
        'correo',
        'calle',
        'colonia',
        'municipio',
        'estado'
    ];

    public function usuario(){
        return $this->belongsTo('App\Usuario','id_asociado','id_asociado');
    }
//    public function usuario(){
//        return $this->belongsToMany('App\Usuario','id_asociado','id_asociado');
//    }
}
